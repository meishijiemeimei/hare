# RPG Engine

A RPG engine for Haxe including the following core components while the visuals and user input are abstracted away:

- 2D Map
- Events (Scripted with Lua)
- Items

A default implementation for HaxeFlixel is provided. 
But it could be easily implemented on other game frameworks.

## Usage with HaxeFlixel

1. Run a command (TBC) to generate the game binary. 
2. Add game assets (config, graphics, scripts, etc...) to the generated assets folder
3. Run the game and test

## Implement on other game frameworks

Implement the follow interfaces:
- TBC
- TBC